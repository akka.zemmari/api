FROM node:14
WORKDIR /usr/src/app
COPY . .
RUN npm install
EXPOSE 4321
CMD [ "nodemon", "-r", "dotenv/config", "server.js", "dotenv_config_path=./env/production.env" ]