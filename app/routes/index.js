const bien = require("./bien.routes");
const images = require("./images.routes");
const users = require("./users.routes");
const address = require("./address.routes");
const register = require("./register.routes");
const bail = require("./bail.routes");
const auth = require("./auth.routes");
const pieces = require("./pieces.routes");
const file = require("./file.routes");

module.exports = {
    users,
    address,
    register,
    bien,
    images,
    file,
    bail,
    auth,
    pieces
}
