"use strict";

const express = require('express');
const middlewares = require('../middlewares');
const controllers = require('../controllers');

const router = express.Router();

// router.get('/:userId', middlewares.auth.isAuthentifiedAdmin, controllers.users.getUser);

router.put('/editProfile', middlewares.auth.isAuthentified, controllers.users.updateUser);
router.get('/listeProprietaire', controllers.users.listeProprietaire);

router.post('/', controllers.users.addUser);

router.delete("/:mail", controllers.users.deleteUser);

router.get('/', controllers.users.getListUsers);

router.get('/role/:idRole', controllers.users.getListUsersWithIdRole);

router.get('/emailRole/:idRole', controllers.users.getListUsersEmailWithIdRole);

router.delete("/:userId", controllers.users.deleteUser);

router.get('/', controllers.users.getListUsers);

router.get('/role/:idRole', controllers.users.getListUsersWithIdRole);

router.get('/:userId', controllers.users.getUser);

module.exports = router;
