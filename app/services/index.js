const exception = require("./exception.service");
const security = require("./security.service");
const bien = require("./bien.service");
const images = require("./images.service");
const file = require("./file.service");
const users = require("./users.service");
const address = require("./address.service");
const register = require("./register.service");
const bail = require("./bail.service");
const pdf = require("./pdf.service");
const adresse = require("./adresse.service");
const auth = require("./auth.service");
const pieces = require("./pieces.service");

module.exports = {
    exception,
    security,
    users,
    address,
    register,
    bien,
    images,
    file,
    users,
    bail,
    pdf,
    adresse,
    auth,
    pieces
};
