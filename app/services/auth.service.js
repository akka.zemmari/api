"use strict";

const { transaction } = require('../../database');

/**
 * Récupère les informations de l'administrateur
 * @param {*} mail Nom d'utilisateur de l'administrateur
 * @returns une erreur ou les informations de l'administrateur
 */
const loginAdmin = async (mail) => {
	const query = 'SELECT * ' +
		'FROM user_view ' +
		'WHERE USER_mail = ? ' +
		'AND (ROLE_id = 1 ' +
		'or ROLE_id = 2);';
	let [queryRes, fields] = [];

	await transaction(async connection => {
		try {
			[queryRes, fields] = await connection.query(query, mail);
		} catch (err) {
			throw new Error(err);
		}
	})
		.catch((err) => {
			throw err;
		});

	return queryRes;
};

/**
 * Récupère les informations de l'administrateur
 * @param {*} mail Nom d'utilisateur de l'administrateur
 * @returns une erreur ou les informations de l'administrateur
 */
 const loginUser = async (mail) => {
	const query = 'SELECT * ' +
		'FROM user_view ' +
		'WHERE USER_mail = ? ' +
		'AND (ROLE_id = 3 ' +
		'or ROLE_id = 4);';
	let [queryRes, fields] = [];

	await transaction(async connection => {
		try {
			[queryRes, fields] = await connection.query(query, mail);
		} catch (err) {
			throw new Error(err);
		}
	})
		.catch((err) => {
			throw err;
		});

	return queryRes;
};

module.exports = {
	loginAdmin,
	loginUser
}