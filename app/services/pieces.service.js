"use strict";

const { transaction } = require('../../database');

/**
 * Récupère les pièces du bien spécifié en paramètre
 * @param {*} bienId Identifiant du bien
 * @returns le résultat de la requête
 */
const getPieces = async (bienId) => {
    const query = 'SELECT * ' +
        'FROM piece_bien ' +
        'WHERE idBien = ?';

    let [queryRes, fields] = [];

    await transaction(async connection => {
        try {
            [queryRes, fields] = await connection.query(query, [bienId]);
        } catch (err) {
            throw new Error(err)
        }

    })
        .catch((err) => {
            throw err;
        });

    return queryRes;
};

/**
 * Récupère l'ensemble des types de pièces
 * @returns La liste des types de pièces
 */
const getTypePieces = async () => {
    const query = 'SELECT * ' +
        'FROM type_piece ';

    let [queryRes, fields] = [];

    await transaction(async connection => {
        try {
            [queryRes, fields] = await connection.query(query);
        } catch (err) {
            throw new Error(err)
        }

    })
        .catch((err) => {
            throw err;
        });

    return queryRes;
};


module.exports = {
	getPieces,
    getTypePieces
};