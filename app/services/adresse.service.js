"use strict";

const { transaction } = require('../../database');

/**
 * Modifie le contenu d'une actualité
 * @param {*} newsId Identifiant de l'actualité
 * @returns Une 200 ou une erreur
 */
const updateUserAdresse = async (newUser) => {
    const query = 'UPDATE adresse ' +
        'SET ? ' +
        'WHERE id = ?'

    let [queryRes, fields] = [];
    const params = [
        {
            id: newUser.adresse.id,
            numero: newUser.adresse.numero,
            rue: newUser.adresse.rue,
            complement: newUser.adresse.complement,
            codePostal: newUser.adresse.codePostal,
            ville: newUser.adresse.ville,
            pays: newUser.adresse.pays,
        },
        newUser.adresse.id
    ];
    await transaction(async connection => {
        try {
            [queryRes, fields] = await connection.query(query, params);
        } catch (err) {
            throw new Error(err);
        }
    })
        .catch((err) => {
            throw err;
        });

    return queryRes;
}

module.exports = {
    updateUserAdresse,
}
