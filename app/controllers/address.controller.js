"use strict";

const services = require('../services');

/**
 * Récupère l'adresse dont l'id est passé en paramètre de la route
 * @param {*} req Requête
 * @param {*} res Réponse
 * @returns L'adresse demandé
 */
const getAddress = async (req, res) => {
    let foundAddress = [];

    try {
        foundAddress = await services.address.getAddress(req.params.addressId);
    } catch (err) {
        return services.exception.generateException(err, res);
    }

    res.status(200).json(foundAddress);
}

/**
 * Ajoute une nouvelle adresse
 * @param {*} req Requête
 * @param {*} res Réponse
 * @returns L'erreur retournée par le service ou une confirmation (code 200)
 */
const addAddress = async (req, res) => {
    let address = req.body.address;

    try {
        await services.address.addAddress(address);
    } catch (err) {
        return services.exception.generateException(err, res);
    }

    res.status(200).json();
}

/**
 * Retourne le dernier id de la table adresse (dernière adresse créée)
 * @param {*} req Requête
 * @param {*} res Réponse
 * @returns le résultat de la requête, ici le dernier id
 */
const getLastIdCreated = async (req, res) => {
    let foundLastId = [];

    try {
        foundLastId = await services.address.getLastIdCreated();
    } catch (err) {
        return services.exception.generateException(err, res);
    }

    res.status(200).json(foundLastId);
}

module.exports = {
    getAddress,
    addAddress,
    getLastIdCreated
}