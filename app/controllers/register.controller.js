"use strict";

const services = require('../services');

/**
 * Ajoute un nouvel utilisateur en base (ainsi que son adresse associée)
 * @param {*} req Requête
 * @param {*} res Réponse
 * @returns L'erreur retournée par le service ou une confirmation (code 200)
 */
const register = async (req, res) => {
    let user = req.body.user;
    let address = req.body.address;
	try {
		await services.register.register(address, user);
	} catch (err) {
		return services.exception.generateException(err, res);
	}

	res.status(200).json();
}

module.exports = {
    register
}