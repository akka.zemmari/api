"use strict";

const services = require('../services');

/**
 * Crée un nouveau bail
 * @param {*} req Requête
 * @param {*} res Réponse
 * @returns L'erreur retournée par le service ou le bail trouvé
 */
 const postBail = async (req, res) => {
	let idBail = -1;
	try {
		idBail = await services.bail.postBail(req.body.bail);
		
		if (req.body.bail.pdf) {
			req.body.bail.pdf = await services.pdf.savePdf(req.body.bail.pdf, './document-location/dossier-numero-' + idBail.insertId + '/1.bail/');
			await services.bail.updateBailPdf(idBail.insertId, req.body.bail.pdf);
		}
	} catch (err) {
		return services.exception.generateException(err, res);
	}
	res.status(200).json();
};

/**
 * Récupère l'ensemble des bails
 * @param {*} req Requête
 * @param {*} res Réponse
 * @returns L'erreur retournée par le service ou les bails trouvés
 */
const getBails = async (req, res) => {
	let queryRes = null;

	try {
		queryRes = await services.bail.getBails();
	} catch (err) {
		return services.exception.generateException(err, res);
	}

	res.status(200).json(queryRes);
};

/**
 * Récupère les bails d'un Agent
 * @param {*} req Requête
 * @param {*} res Réponse
 * @returns L'erreur retournée par le service ou les bails trouvés
 */
const getBailAgent = async (req, res) => {
	let foundBail = [];

	try {
		foundBail = await services.bail.getBailAgent(req.params.bailId);
	} catch (err) {
		return services.exception.generateException(err, res);
	}
	res.status(200).json(foundBail);
};

/**
 * Récupère les bails d'un propriétaire
 * @param {*} req Requête
 * @param {*} res Réponse
 * @returns L'erreur retournée par le service ou les bails trouvés
 */
const getBailProprietaireId = async (req, res) => {
	let foundBail = [];

	try {
		foundBail = await services.bail.getBailProprietaireId(req.params.userId);
	} catch (err) {
		return services.exception.generateException(err, res);
	}
	res.status(200).json(foundBail);
};

/**
 * Récupère les bails d'un locataire
 * @param {*} req Requête
 * @param {*} res Réponse
 * @returns L'erreur retournée par le service ou les bails trouvés
 */
const getBailLocataireId = async (req, res) => {
	let foundBail = [];

	try {
		foundBail = await services.bail.getBailLocataireId(req.params.userId);
	} catch (err) {
		return services.exception.generateException(err, res);
	}
	res.status(200).json(foundBail);
};

module.exports = {
    postBail,
	getBailAgent,
	getBailProprietaireId,
	getBailLocataireId,
	getBails,
	getBailAgent
}